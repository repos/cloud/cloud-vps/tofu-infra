terraform {
  required_providers {
    openstack = {
      source  = "registry.opentofu.org/terraform-provider-openstack/openstack"
      version = "3.0.0"
    }
  }
}
