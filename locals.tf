locals {
  common_desc = "managed by tofu-infra"
  deployment  = trimsuffix(var.cloudvps_region, "-r")
}

# TODO: use 'tofu state mv' to inject the project string into the resource name (in the map key)
# before we introduce tenant networks, so we avoid name clashes of network objects with the same
# tofu name in different projects. This applies to 'networks', 'subnets', 'routers', 'ports', and 'router_interfaces'

locals {
  network_files = fileset("resources/${var.cloudvps_region}/", "*/network/networks.yaml")

  network_projects = [for file in local.network_files : element(split("/", file), length(split("/", file)) - 3)]

  projects_networks = {
    for project in local.network_projects :
    project => yamldecode(file("resources/${var.cloudvps_region}/${project}/network/networks.yaml"))
  }

  project_network_keys = {
    for project_key, networks in local.projects_networks : project_key => keys(networks)
  }

  networks = merge([
    for project_key, network_keys in local.project_network_keys : {
      for network_key in network_keys :
      "${project_key}_${network_key}" => local.projects_networks[project_key][network_key]
    }
  ]...)
}

locals {
  subnet_files = fileset("resources/${var.cloudvps_region}/", "*/network/subnets.yaml")

  subnet_projects = [for file in local.subnet_files : element(split("/", file), length(split("/", file)) - 3)]

  projects_subnets = {
    for project in local.subnet_projects :
    project => yamldecode(file("resources/${var.cloudvps_region}/${project}/network/subnets.yaml"))
  }

  project_subnet_keys = {
    for project_key, subnets in local.projects_subnets : project_key => keys(subnets)
  }

  subnets = merge([
    for project_key, subnet_keys in local.project_subnet_keys : {
      for subnet_key in subnet_keys :
      "${project_key}_${subnet_key}" => local.projects_subnets[project_key][subnet_key]
    }
  ]...)
}

locals {
  router_files = fileset("resources/${var.cloudvps_region}/", "*/network/routers.yaml")

  router_projects = [for file in local.router_files : element(split("/", file), length(split("/", file)) - 3)]

  projects_routers = {
    for project in local.router_projects :
    project => yamldecode(file("resources/${var.cloudvps_region}/${project}/network/routers.yaml"))
  }

  project_router_keys = {
    for project_key, routers in local.projects_routers : project_key => keys(routers)
  }

  routers = merge([
    for project_key, router_keys in local.project_router_keys : {
      for router_key in router_keys :
      "${project_key}_${router_key}" => local.projects_routers[project_key][router_key]
    }
  ]...)
}

locals {
  port_files = fileset("resources/${var.cloudvps_region}/", "*/network/ports.yaml")

  port_projects = [for file in local.port_files : element(split("/", file), length(split("/", file)) - 3)]

  projects_ports = {
    for project in local.port_projects :
    project => yamldecode(file("resources/${var.cloudvps_region}/${project}/network/ports.yaml"))
  }

  project_port_keys = {
    for project_key, ports in local.projects_ports : project_key => keys(ports)
  }

  ports = merge([
    for project_key, port_keys in local.project_port_keys : {
      for port_key in port_keys :
      "${project_key}_${port_key}" => local.projects_ports[project_key][port_key]
    }
  ]...)
}

locals {
  router_interface_files = fileset("resources/${var.cloudvps_region}/", "*/network/router_interfaces.yaml")

  router_interface_projects = [for file in local.router_interface_files : element(split("/", file), length(split("/", file)) - 3)]

  projects_router_interfaces = {
    for project in local.router_interface_projects :
    project => yamldecode(file("resources/${var.cloudvps_region}/${project}/network/router_interfaces.yaml"))
  }

  project_router_interface_keys = {
    for project_key, router_interfaces in local.projects_router_interfaces : project_key => keys(router_interfaces)
  }

  router_interfaces = merge([
    for project_key, router_interface_keys in local.project_router_interface_keys : {
      for router_interface_key in router_interface_keys :
      "${project_key}_${router_interface_key}" => local.projects_router_interfaces[project_key][router_interface_key]
    }
  ]...)
}

locals {
  projects = yamldecode(file("projects_${local.deployment}.yaml"))
}

locals {
  zone_files = fileset("resources/${var.cloudvps_region}/", "*/dns/*.yaml")

  zones = {
    for zone_file in local.zone_files :
    # NOTE: retain the trailing "." dot
    trimsuffix(basename(zone_file), "yaml") =>
    merge(yamldecode(file("resources/${var.cloudvps_region}/${zone_file}")),
      { project = split("/", dirname(zone_file))[0] },
    )
  }

  zones_to_import = {
    for zone_key, zone_value in local.zones :
    zone_key => zone_value
    if contains(keys(zone_value), "import_id")
  }
}

# NOTE: this creates a map like this:
# {
#   "1.example.com" = {               <-- the zone reference
#       "something.1.example.com" = { <-- the record itself
#           type = "A" ...
#       }
#       "other.1.example.com" = {     <-- nother record
#           type = "CNAME" ...
#       }
#   },
#   "2.example.com" = {               <-- the zone reference
#       "something.2.example.com" = { <-- the record itself
#           type = "A" ...
#       }
#   }
# }
# which is a somewhat convenient bridge abstraction between our YAML data structure and the openstack tofu provider
locals {
  records = {
    for zone_key, zone_data in local.zones :
    zone_key => zone_data.records
    if zone_data.records != null
  }
}

# NOTE: this weird remap results in a structure like this:
# [
#   {
#     project = "project-a",
#     zone = "1.example.com.",
#     record_key = "something.1.example.com.",
#     import_id = "123123",
#   },
#   {
#     project = "project-a",
#     zone = "2.example.com.",
#     record_key = "something.2.example.com.",
#     import_id = "123123",
#   }
# ]
# which is very convenient for the import {} block
locals {
  records_to_import = flatten([
    for zone_key, zone_data in local.zones : [
      for record_key, record_value in zone_data.records : {
        project    = zone_data.project,
        zone       = zone_key,
        record_key = record_key,
        import_id  = record_value.import_id,
      }
      if contains(keys(record_value), "import_id")
    ]
    if zone_data.records != null
  ])
}

locals {
  secgroup_files = fileset("resources/${var.cloudvps_region}/", "*/network/secgroups.yaml")

  secgroups_projects = [for file in local.secgroup_files : element(split("/", file), length(split("/", file)) - 3)]

  projects_secgroups = {
    for project in local.secgroups_projects :
    project => yamldecode(file("resources/${var.cloudvps_region}/${project}/network/secgroups.yaml"))
  }

  projects_with_default_secgroups = {
    for project_name, project_def in local.projects :
    project_name => project_def if lookup(project_def, "manage_default_secgroup", true) == true
  }

  default_secgroup = yamldecode(file("./templates/network/secgroups/${var.cloudvps_region}_default.yaml"))
}

locals {
  custom_secgroups = {
    for sg in flatten([for project, sglist in local.projects_secgroups : [for sg in sglist : sg]]) :
    "${sg.project}_${sg.name}" => sg
    # NOTE: ignore any potential 'default' secgroups defined in the data YAMLs, it is created by neutron
    if sg.name != "default"
  }

  default_secgroups = {
    for project_key, project_def in local.projects_with_default_secgroups :
    "${project_key}_default" => merge(local.default_secgroup.default, { project = project_key })
  }

  # NOTE: only used below, to generate the secgroup_rules mapping
  all_secgroups = merge(local.custom_secgroups, local.default_secgroups)
}

locals {
  secgroups_to_import = local.custom_secgroups
}

locals {
  secgroups_rules = merge([
    for rule in flatten([
      for k, secgroup in local.all_secgroups : [
        for i, rule in secgroup.rules : {
          "${secgroup.project}_${secgroup.name}_${i}" = {
            project  = secgroup.project,
            secgroup = secgroup.name,
            rule     = rule,
          }
        }
      ]
    ]) : rule
  ]...)
}
