# see https://opentofu.org/docs/language/import/
# import blocks are only allowed in the root module

import {
  for_each = local.zones_to_import

  to = module.zones.openstack_dns_zone_v2.zone[each.key]
  id = "${each.value.import_id}:${module.projects.projects[each.value.project].id}"
}

import {
  for_each = local.records_to_import

  to = module.records[each.value.zone].openstack_dns_recordset_v2.record[each.value.record_key]
  id = "${module.projects.projects[each.value.project].id}/${module.zones.zones[each.value.zone].id}/${each.value.import_id}"
}

import {
  for_each = local.secgroups_to_import

  to = module.secgroups.openstack_networking_secgroup_v2.secgroup["${each.value.project}_${each.value.name}"]
  id = each.value.import_id
}
