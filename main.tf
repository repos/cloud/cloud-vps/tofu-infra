provider "openstack" {}

variable "cloudvps_region" {
  type = string
}

module "cloudvps_flavors" {
  source          = "./modules/cloudvps_flavors"
  cloudvps_region = var.cloudvps_region
}

module "networks" {
  source   = "./modules/networks"
  networks = local.networks
  projects = module.projects.projects
}

module "subnets" {
  source   = "./modules/subnets"
  subnets  = local.subnets
  networks = module.networks.networks
  projects = module.projects.projects
}

module "routers" {
  source   = "./modules/routers"
  routers  = local.routers
  networks = module.networks.networks
  subnets  = module.subnets.subnets
  projects = module.projects.projects
}

module "ports" {
  source   = "./modules/ports"
  ports    = local.ports
  networks = module.networks.networks
  subnets  = module.subnets.subnets
  routers  = module.routers.routers
  projects = module.projects.projects
}

module "router_interfaces" {
  source            = "./modules/router_interfaces"
  router_interfaces = local.router_interfaces
  ports             = module.ports.ports
  subnets           = module.subnets.subnets
  routers           = module.routers.routers
}

module "secgroups" {
  source                          = "./modules/secgroups"
  custom_secgroups                = local.custom_secgroups
  secgroups_rules                 = local.secgroups_rules
  projects_with_default_secgroups = local.projects_with_default_secgroups
  projects                        = module.projects.projects
  common_desc                     = local.common_desc
}

module "projects" {
  source   = "./modules/projects"
  projects = local.projects
}

module "zones" {
  source      = "./modules/zones"
  zones       = local.zones
  projects    = module.projects.projects
  common_desc = local.common_desc
}

module "records" {
  source      = "./modules/records"
  for_each    = local.records
  zone_id     = module.zones.zones[each.key].id
  project_id  = module.zones.zones[each.key].project_id
  records     = each.value
  common_desc = local.common_desc
}
