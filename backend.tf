terraform {
  backend "s3" {
    bucket = "admin:tofu-state"
    key    = "repos/cloud/cloud-vps/tofu-infra"

    skip_region_validation      = true
    skip_credentials_validation = true
    use_path_style              = true
  }
}
