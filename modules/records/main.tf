variable "zone_id" {}
variable "project_id" {}
variable "common_desc" { type = string }

# see https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/dns_recordset_v2
variable "records" {
  type = map(object({
    import_id   = optional(string, null), # used in the root module, not here
    name        = string,
    type        = string,
    records     = list(string),
    description = optional(string, ""),
    ttl         = optional(number, 3600),
    # may make tofu run faster, the records will be considered created/updated as soon as the request returns
    disable_status_check = optional(bool, true),
  }))
}

resource "openstack_dns_recordset_v2" "record" {
  for_each = var.records

  name                 = each.value.name
  zone_id              = var.zone_id
  project_id           = var.project_id
  type                 = each.value.type
  records              = each.value.records
  description          = "%{if each.value.description != ""}${each.value.description} - ${var.common_desc}%{else}${var.common_desc}%{endif}"
  ttl                  = each.value.ttl
  disable_status_check = each.value.disable_status_check
}

output "records" {
  value = openstack_dns_recordset_v2.record
}
