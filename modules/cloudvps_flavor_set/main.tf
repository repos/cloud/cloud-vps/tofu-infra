variable "base_settings" {
  type = map(string)
}

variable "cloudvps_region" {
  type = string
}

variable "flavors" {
  type = map(object({
    ram       = number,
    vcpus     = number,
    disk      = number,
    ephemeral = optional(number, 0),
    extra     = optional(map(string), {}),
    projects  = optional(map(set(string)), {}),
  }))
}

locals {
  flavor_project_access = flatten([
    for flavor_key, flavor in var.flavors : [
      for project in try(flavor.projects[var.cloudvps_region], []) : {
        flavor  = flavor_key,
        project = project,
      }
    ]
  ])
}

# https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/compute_flavor_v2
resource "openstack_compute_flavor_v2" "flavor" {
  for_each = var.flavors

  name      = each.key
  ram       = each.value.ram
  vcpus     = each.value.vcpus
  disk      = each.value.disk
  ephemeral = each.value.ephemeral

  is_public   = length(each.value.projects) == 0
  extra_specs = merge(var.base_settings, each.value.extra)
}

resource "openstack_compute_flavor_access_v2" "flavor_private_access" {
  for_each = tomap({
    for flavor_project in local.flavor_project_access : "${flavor_project.flavor}.${flavor_project.project}" => flavor_project
  })

  tenant_id = each.value.project
  flavor_id = openstack_compute_flavor_v2.flavor[each.value.flavor].id
}
