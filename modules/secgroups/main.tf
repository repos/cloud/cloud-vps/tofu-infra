variable "common_desc" { type = string }

variable "projects" {}

variable "custom_secgroups" {
  type = map(object({
    project     = string,
    name        = string,
    import_id   = optional(string, null),
    description = optional(string, ""),
  }))
}

# see:
# https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_secgroup_v2
resource "openstack_networking_secgroup_v2" "secgroup" {
  for_each = var.custom_secgroups

  tenant_id   = var.projects[each.value.project].id
  name        = each.value.name
  description = "%{if each.value.description != ""}${each.value.description} - ${var.common_desc}%{else}${var.common_desc}%{endif}"

  # set delete_default_rules if this secgroup is being created, not imported
  # if imported, we most likely wrote all the rules in the yaml, no need to delete anything
  # if not imported, the secgroup is natively created via tofu, and we need to delete
  # any additional rule that neutron may add, otherwise such additional rules wont be tracked by tofu
  # use 'null' in the false case so the provider doesn't detect a change
  delete_default_rules = each.value.import_id == null ? true : null
}

output "secgroups" {
  value = openstack_networking_secgroup_v2.secgroup
}

variable "secgroups_rules" {
  type = map(object({
    project  = string,
    secgroup = string,
    rule = object({
      description       = optional(string, ""),
      direction         = string,
      ethertype         = string,
      protocol          = optional(string),
      port_range_min    = optional(number),
      port_range_max    = optional(number),
      remote_ip_prefix  = optional(string),
      remote_group_name = optional(string),
    })
  }))
}

variable "projects_with_default_secgroups" {}

# see:
# https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/data-sources/networking_secgroup_v2
data "openstack_networking_secgroup_v2" "default" {
  for_each  = var.projects_with_default_secgroups
  name      = "default"
  tenant_id = var.projects[each.key].id
}

# NOTE:
# merge all custom and default sg together with a mapping to their id, so we can query it
# when creating secgroup_rules below
locals {
  custom_secgroup_ids = {
    for sg_key, sg_val in var.custom_secgroups :
    "${sg_key}" => { id = openstack_networking_secgroup_v2.secgroup[sg_key].id }
  }
  default_secgroup_ids = {
    for sg_key, sg_val in data.openstack_networking_secgroup_v2.default :
    "${sg_key}_default" => { id = sg_val.id }
  }
  all_secgroups_ids = merge(local.custom_secgroup_ids, local.default_secgroup_ids)
}

# see:
# https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_secgroup_rule_v2
resource "openstack_networking_secgroup_rule_v2" "secgroup_rule" {
  for_each = var.secgroups_rules

  tenant_id         = var.projects[each.value.project].id
  security_group_id = local.all_secgroups_ids["${each.value.project}_${each.value.secgroup}"].id
  description       = "%{if each.value.rule.description != ""}${each.value.rule.description} - ${var.common_desc}%{else}${var.common_desc}%{endif}"
  direction         = each.value.rule.direction
  ethertype         = each.value.rule.ethertype
  protocol          = each.value.rule.protocol
  port_range_min    = each.value.rule.port_range_min
  port_range_max    = each.value.rule.port_range_max
  remote_ip_prefix  = each.value.rule.remote_ip_prefix
  remote_group_id   = try(local.all_secgroups_ids["${each.value.project}_${each.value.rule.remote_group_name}"].id, null)
}

output "secgroups_rules" {
  value = openstack_networking_secgroup_rule_v2.secgroup_rule
}
