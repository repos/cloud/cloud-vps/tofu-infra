variable "projects" {}

# see https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_network_v2
variable "networks" {
  type = map(object({
    import_id      = optional(string, null), # used in the root module, not here
    shared         = bool,
    external       = bool,
    project_name   = string,
    admin_state_up = optional(bool, true),
    description    = optional(string, null),
    segments = list(object({
      physical_network = string,
      network_type     = string,
      segmentation_id  = optional(number, null),
    }))
  }))
}

resource "openstack_networking_network_v2" "network" {
  for_each = var.networks

  name           = trimprefix(each.key, "${each.value.project_name}_")
  description    = each.value.description
  shared         = each.value.shared
  external       = each.value.external
  tenant_id      = var.projects[each.value.project_name].id
  admin_state_up = each.value.admin_state_up

  dynamic "segments" {
    for_each = each.value.segments

    content {
      physical_network = segments.value.physical_network
      network_type     = segments.value.network_type
      segmentation_id  = segments.value.segmentation_id
    }
  }
}

output "networks" {
  value = openstack_networking_network_v2.network
}
