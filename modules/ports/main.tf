variable "projects" {}
variable "networks" {}
variable "subnets" {}
variable "routers" {}

variable "ports" {
  # NOTE: there are a few more parameters here, but I'm ignoring them for now.
  # We will most likely need to add them later. Some of them are:
  # * allowed_address_pairs (something we use for VRRP/keepalived)
  # * security_groups_ids (to apply firewalling to a port)
  type = map(object({
    import_id      = optional(string, null), # used in the root module, not here
    description    = optional(string, null),
    admin_state_up = optional(bool, true),
    project_name   = string,
    network_name   = string,
    # NOTE: router ports usually disable port security, but the API defaults to true here
    port_security_enabled = optional(bool, true),
    fixed_ips = optional(list(object({
      subnet_name = string,
      ip_address  = string,
    })), null),
  }))
}

# see:
# https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_port_v2
resource "openstack_networking_port_v2" "port" {
  for_each = var.ports

  name                  = trimprefix(each.key, "${each.value.project_name}_")
  description           = each.value.description
  admin_state_up        = each.value.admin_state_up
  tenant_id             = var.projects[each.value.project_name].id
  network_id            = var.networks["${each.value.project_name}_${each.value.network_name}"].id
  port_security_enabled = each.value.port_security_enabled

  dynamic "fixed_ip" {
    for_each = each.value.fixed_ips

    content {
      ip_address = fixed_ip.value.ip_address
      subnet_id  = var.subnets["${each.value.project_name}_${fixed_ip.value.subnet_name}"].id
    }
  }
}

output "ports" {
  value = openstack_networking_port_v2.port
}
