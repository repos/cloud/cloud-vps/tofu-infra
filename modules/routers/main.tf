variable "projects" {}
variable "networks" {}
variable "subnets" {}

variable "routers" {
  type = map(object({
    import_id      = optional(string, null), # used in the root module, not here
    project_name   = string,
    description    = optional(string, null),
    admin_state_up = optional(bool, true),
    enable_snat    = optional(bool, false),
    external_gateway_info = optional(object({
      network_name = optional(string, null),
      external_fixed_ips = optional(list(object({
        ip_address  = string,
        subnet_name = string,
      })), null)
      }), {
      # provide a default so the dynamic for_each doesn't complain
      external_fixed_ips = []
    }),
  }))
}

# see:
# https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_router_v2

resource "openstack_networking_router_v2" "router" {
  for_each = var.routers

  name                = trimprefix(each.key, "${each.value.project_name}_")
  tenant_id           = var.projects[each.value.project_name].id
  description         = each.value.description
  admin_state_up      = each.value.admin_state_up
  external_network_id = try(var.networks["${each.value.project_name}_${each.value.external_gateway_info.network_name}"].id, null)
  enable_snat         = each.value.enable_snat

  dynamic "external_fixed_ip" {
    for_each = each.value.external_gateway_info.external_fixed_ips

    content {
      ip_address = external_fixed_ip.value.ip_address
      subnet_id  = var.subnets["${each.value.project_name}_${external_fixed_ip.value.subnet_name}"].id
    }
  }
}

output "routers" {
  value = openstack_networking_router_v2.router
}
