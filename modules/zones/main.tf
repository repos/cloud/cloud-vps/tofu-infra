variable "projects" {}
variable "common_desc" { type = string }

# see https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/dns_zone_v2
variable "zones" {
  type = map(object({
    import_id   = optional(string, null), # used in the root module, not here
    project     = string,
    email       = optional(string, "root@wmcloud.org"),
    description = optional(string, ""),
    type        = optional(string, "PRIMARY"), # PRIMARY or SECONDARY
    ttl         = optional(number, null),
    # may make tofu run faster, the zone will be considered created/updated as soon as the request returns
    disable_status_check = optional(bool, true),
  }))
}

resource "openstack_dns_zone_v2" "zone" {
  for_each = var.zones

  name                 = each.key
  project_id           = var.projects[each.value.project].id
  email                = each.value.email
  description          = "%{if each.value.description != ""}${each.value.description} - ${var.common_desc}%{else}${var.common_desc}%{endif}"
  type                 = each.value.type
  ttl                  = each.value.ttl
  disable_status_check = each.value.disable_status_check
}

output "zones" {
  value = openstack_dns_zone_v2.zone
}
