variable "subnets" {}
variable "routers" {}
variable "ports" {}

variable "router_interfaces" {
  type = map(object({
    import_id    = optional(string, null), # used in the root module, not here
    router_name  = string,
    subnet_name  = optional(string, null), # undocumented mutually exclusive with port
    port_name    = string,
    project_name = string,
  }))
}

# see:
# https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_router_interface_v2
resource "openstack_networking_router_interface_v2" "router_interface" {
  for_each = var.router_interfaces

  router_id = var.routers["${each.value.project_name}_${each.value.router_name}"].id
  subnet_id = try(var.subnets["${each.value.project_name}_${each.value.subnet_name}"].id, null)
  port_id   = var.ports["${each.value.project_name}_${each.value.port_name}"].id
}

output "router_interfaces" {
  value = openstack_networking_router_interface_v2.router_interface
}
