variable "cloudvps_region" {
  type = string
}

module "flavors_g4" {
  source = "../cloudvps_flavor_set"

  cloudvps_region = var.cloudvps_region

  base_settings = {
    "aggregate_instance_extra_specs:ceph"          = "true"
    "aggregate_instance_extra_specs:network-agent" = "ovs"
    "quota:disk_read_iops_sec"                     = "5000"
    "quota:disk_write_iops_sec"                    = "500"
    "quota:disk_total_bytes_sec"                   = "200000000"
  }

  flavors = {
    # Public flavors
    "g4.cores1.ram1.disk20" = {
      vcpus = 1
      ram   = 1024
      disk  = 20
    }
    "g4.cores1.ram2.disk20" = {
      vcpus = 1
      ram   = 2048
      disk  = 20
    }
    "g4.cores2.ram4.disk20" = {
      vcpus = 2
      ram   = 4096
      disk  = 20
    }
    "g4.cores4.ram8.disk20" = {
      vcpus = 4
      ram   = 8192
      disk  = 20
    }
    "g4.cores8.ram16.disk20" = {
      vcpus = 8
      ram   = 16384
      disk  = 20
    }
    "g4.cores4.ram16.disk20" = {
      vcpus = 4
      ram   = 16384
      disk  = 20
    }
    "g4.cores4.ram32.disk20" = {
      vcpus = 4
      ram   = 32768
      disk  = 20
    }
    "g4.cores8.ram32.disk20" = {
      vcpus = 8
      ram   = 32768
      disk  = 20
    }
    "g4.cores16.ram16.disk20" = {
      vcpus = 16
      ram   = 16384
      disk  = 20
    }
    "g4.cores16.ram32.disk20" = {
      vcpus = 16
      ram   = 32768
      disk  = 20
    }

    # Per-project flavors
    "g4.cores2.ram8.disk40" = {
      vcpus = 2
      ram   = 8192
      disk  = 40

      projects = {
        "eqiad1-r" = ["wmf-research-tools"]
      }
    }

    "g4.cores4.ram8.disk20.ephem40" = {
      vcpus     = 4
      ram       = 8192
      disk      = 20
      ephemeral = 40

      projects = {
        "eqiad1-r"    = ["tools", "toolsbeta"]
        "codfw1dev-r" = ["tools-codfw1dev"]
      }
    }
    "g4.cores8.ram16.disk20.ephem140" = {
      vcpus     = 8
      ram       = 16384
      disk      = 20
      ephemeral = 120

      projects = {
        "eqiad1-r"    = ["tools", "toolsbeta"]
        "codfw1dev-r" = ["tools-codfw1dev"]
      }
    }
    "g4.cores16.ram64.disk20.10xiops" = {
      vcpus = 16
      ram   = 65536
      disk  = 20

      extra = {
        "quota:disk_read_iops_sec"   = "50000"
        "quota:disk_write_iops_sec"  = "5000"
        "quota:disk_total_bytes_sec" = "2000000000"
      }

      projects = {
        "eqiad1-r"    = ["tools", "toolsbeta"]
        "codfw1dev-r" = ["tools-codfw1dev"]
      }
    }
    "g4.cores16.ram64.disk20.maxiops" = {
      vcpus = 16
      ram   = 65536
      disk  = 20

      extra = {
        "quota:disk_read_iops_sec"   = "5000000"
        "quota:disk_write_iops_sec"  = "5000000"
        "quota:disk_total_bytes_sec" = "200000000000"
      }

      projects = {
        "eqiad1-r"    = ["tools", "toolsbeta"]
        "codfw1dev-r" = ["tools-codfw1dev"]
      }
    }
    "g4.cores8.ram24.disk20.ephemeral40.4xiops" = {
      vcpus     = 8
      ram       = 24576
      disk      = 20
      ephemeral = 40

      extra = {
        "quota:disk_read_iops_sec"   = "20000"
        "quota:disk_write_iops_sec"  = "2000"
        "quota:disk_total_bytes_sec" = "800000000"
      }

      projects = {
        "eqiad1-r" = ["gitlab-runners", "integration"]
      }
    }
    "g4.cores32.ram64.disk20" = {
      vcpus = 32
      ram   = 65536
      disk  = 20

      projects = {
        "eqiad1-r" = ["wikitextexp"]
      }
    }
    "g4.cores24.ram122.disk20" = {
      vcpus = 24
      ram   = 124928
      disk  = 20

      projects = {
        "eqiad1-r" = ["wikiwho"]
      }
    }
    "g4.cores8.ram36.disk20.4xiops" = {
      vcpus = 8
      ram   = 36864
      disk  = 20

      extra = {
        "quota:disk_read_iops_sec"   = "20000"
        "quota:disk_write_iops_sec"  = "2000"
        "quota:disk_total_bytes_sec" = "800000000"
      }

      projects = {
        "eqiad1-r" = ["integration"]
      }
    }
    "g4.cores8.ram24.disk20.ephemeral60.4xiops" = {
      vcpus     = 8
      ram       = 24576
      disk      = 20
      ephemeral = 60

      extra = {
        "quota:disk_read_iops_sec"   = "20000"
        "quota:disk_write_iops_sec"  = "2000"
        "quota:disk_total_bytes_sec" = "800000000"
      }

      projects = {
        "eqiad1-r" = ["integration"]
      }
    }
    "g4.cores8.ram24.disk20.ephemeral90.4xiops" = {
      vcpus     = 8
      ram       = 24576
      disk      = 20
      ephemeral = 90

      extra = {
        "quota:disk_read_iops_sec"   = "20000"
        "quota:disk_write_iops_sec"  = "2000"
        "quota:disk_total_bytes_sec" = "800000000"
      }

      projects = {
        "eqiad1-r" = ["gitlab-runners", "integration", "search", "wikiapiary"]
      }
    }
    "g4.cores2.ram4.disk20.ephemeral20.4xiops" = {
      vcpus     = 2
      ram       = 4096
      disk      = 20
      ephemeral = 20

      extra = {
        "quota:disk_read_iops_sec"   = "20000"
        "quota:disk_write_iops_sec"  = "2000"
        "quota:disk_total_bytes_sec" = "800000000"
      }

      projects = {
        "eqiad1-r" = ["integration"]
      }
    }
    # T383357
    "g4.cores16.ram48.disk20.ephemeral90.4xiops" = {
      vcpus     = 16
      ram       = 49152
      disk      = 20
      ephemeral = 90

      extra = {
        "quota:disk_read_iops_sec"   = "20000"
        "quota:disk_write_iops_sec"  = "2000"
        "quota:disk_total_bytes_sec" = "800000000"
      }

      projects = {
        "eqiad1-r" = ["integration"]
      }
    }

    # mini-sized flavor for creation of base images with wmcs-image-create
    "g4.cores1.ram2.disk4" = {
      vcpus = 1
      ram   = 2048
      disk  = 4

      projects = {
        "eqiad1-r"    = ["testlabs"]
        "codfw1dev-r" = ["testlabs"]
      }
    }
  }
}


# non-ceph flavors for etcd
module "flavors_g4_local" {
  source = "../cloudvps_flavor_set"

  cloudvps_region = var.cloudvps_region

  base_settings = {
    "aggregate_instance_extra_specs:localdisk"     = "true"
    "aggregate_instance_extra_specs:network-agent" = "ovs"
  }

  flavors = {
    "g4.cores1.ram2.disk20.localdisk" = {
      vcpus = 1
      ram   = 2048
      disk  = 20

      projects = {
        "eqiad1-r"    = ["tools", "toolsbeta", "testlabs"]
        "codfw1dev-r" = ["tools-codfw1dev", "testlabs"]
      }
    }
    "g4.cores2.ram4.disk20.localdisk" = {
      vcpus = 2
      ram   = 4096
      disk  = 20

      projects = {
        "eqiad1-r"    = ["tools", "toolsbeta", "testlabs"]
        "codfw1dev-r" = ["tools-codfw1dev", "testlabs"]
      }
    }
  }
}
