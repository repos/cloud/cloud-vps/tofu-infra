variable "projects" {
  type = map(object({
    description   = optional(string, null),
    domain_id     = optional(string, "default"),
    enabled       = optional(bool, true),
    is_domain     = optional(bool, false),
    parent_id     = optional(string, "default"),
    creation_task = optional(string, "no-task"),
  }))
}

resource "openstack_identity_project_v3" "project" {
  for_each = var.projects

  name        = each.key
  description = each.value.description
  domain_id   = each.value.domain_id
  enabled     = each.value.enabled
  is_domain   = each.value.is_domain
  parent_id   = each.value.parent_id
}

output "projects" {
  value = openstack_identity_project_v3.project
}
