variable "projects" {}
variable "networks" {
}

# see https://registry.terraform.io/providers/terraform-provider-openstack/openstack/latest/docs/resources/networking_subnet_v2
variable "subnets" {
  type = map(object({
    import_id         = optional(string, null), # used in the root module, not here
    network_name      = string,
    cidr              = string,
    project_name      = string,
    description       = optional(string, null),
    gateway_ip        = optional(string, null),
    no_gateway        = optional(bool, null),
    enable_dhcp       = optional(bool, true),
    dns_nameservers   = optional(list(string), null),
    ip_version        = optional(number, 4),
    ipv6_address_mode = optional(string, null),
    ipv6_ra_mode      = optional(string, null),
    allocation_pools = optional(list(object({
      start = string,
      end   = string,
    })), null),
  }))
}

resource "openstack_networking_subnet_v2" "subnet" {
  for_each = var.subnets

  name              = trimprefix(each.key, "${each.value.project_name}_")
  network_id        = var.networks["${each.value.project_name}_${each.value.network_name}"].id
  cidr              = each.value.cidr
  tenant_id         = var.projects[each.value.project_name].id
  description       = each.value.description
  gateway_ip        = each.value.gateway_ip
  no_gateway        = each.value.no_gateway
  enable_dhcp       = each.value.enable_dhcp
  dns_nameservers   = each.value.dns_nameservers
  ip_version        = each.value.ip_version
  ipv6_address_mode = each.value.ipv6_address_mode
  ipv6_ra_mode      = each.value.ipv6_ra_mode

  dynamic "allocation_pool" {
    for_each = each.value.allocation_pools

    content {
      start = allocation_pool.value.start
      end   = allocation_pool.value.end
    }
  }
}

output "subnets" {
  value = openstack_networking_subnet_v2.subnet
}
